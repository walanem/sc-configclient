package br.com.wfigueiredo.controller;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by walanem on 11/04/17.
 */
@RestController
@RefreshScope   // The entire bean is reloaded in a safe manner.
@ConfigurationProperties(prefix = "sfv")
public class ConfigClientController {

    private String character;

    @RequestMapping(value = "/character")
    public String getCharacterOfTheDay() {
        return "Today's Street Fighter V chosen character is: [" + character + "]";
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public String getCharacter() {
        return character;
    }
}